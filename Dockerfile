FROM ubuntu:16.04

MAINTAINER Udhay "udhay1co.de@gmail.com"

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev && \
    pip install --upgrade pip

COPY . /flask_app

WORKDIR /flask_app

RUN pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT [ "python" ]

CMD [ "run.py" ]